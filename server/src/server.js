const express = require("express");
const { graphqlExpress, graphiqlExpress } = require("apollo-server-express");
const cors = require("cors");
const bodyParser = require("body-parser");
const { makeExecutableSchema } = require("graphql-tools");
const { graphql } = require("graphql");
import typeDefs from "./typedefs";
import resolvers from "./resolvers";
import loaders from "./loader";

const schema = makeExecutableSchema({ typeDefs, resolvers });

const app = express();

app.use(cors());

app.use(
    "/graphql",
    bodyParser.json(),
    graphqlExpress(() => ({
      schema,
      context: {
        loaders: loaders()
      }
    }))
);

app.use("/graphiql", graphiqlExpress({ endpointURL: "/graphql" }));

app.listen(4000, () => {
  console.log("Go to http://localhost:4000/graphiql to run queries");
});
